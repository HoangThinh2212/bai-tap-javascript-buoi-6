//Câu 1
function timNguyenDuongNhoNhat() {
  var sum = 0;
  var n = 0;
  while (sum < 10000) {
    //bước nhảy của vòng lặp là n++
    n++;
    sum = sum + n;
  }

  console.log("n: ", n);
  document.getElementById("result1").innerHTML =
    "Số nguyên dương nhỏ nhất: " + n;
}

// Câu 2
function tinhTong() {
  var soX = document.getElementById("soX").value * 1;
  var soN = document.getElementById("soN").value * 1;
  var sum = 0,
    luyThua = 1;

  for (var i = 1; i <= soN; i++) {
    luyThua *= soX;
    sum += luyThua;
  }

  document.getElementById("result2").innerHTML = "Tổng: " + sum;
}

// Câu 3
function giaiThua(n) {
  var giaiThua = 1;
  for (var i = 1; i <= n; i++) {
    giaiThua *= i;
  }
  return giaiThua;
}
function tinhGiaiThua() {
  var soN_GT = document.getElementById("soN_Gt").value * 1;

  document.getElementById("result3").innerHTML =
    "Giai thừa: " + giaiThua(soN_GT);
}

// Câu 4
function taoDiv() {
  var divs = document.getElementsByTagName("p");
  for (var i = 0; i < divs.length; i++) {
    // Vị trí chẵn => màu đỏ
    if ((i+1 ) % 2 == 0) {
      divs[i].innerHTML = "Div chẵn " + (i+1);
      divs[i].style.background = "red";
    } else {
      // Vị trí lẽ => màu xanh
      divs[i].innerHTML = "Div lẻ " + (i+1);
      divs[i].style.background = "DodgerBlue";
    }
  }

  // Thử
  // var content ="";
  // for (var i=1;i<11;i++){
  //   content+=(i%2==0)?`<div class="bg-danger p-3">Div thứ: ${i}</div>`:`<div class="bg-primary p-3">Div thứ: ${i}</div>`;
  // }
  // document.getElementById("result4").innerHTML = content;
}

// BÀI TẬP 1 BT Gợi ý
function chanLe() {
  var stringChan = "";
  var stringLe = "";

  for (var i = 0; i < 100; i++) {
    if (i % 2 == 0) {
      stringChan = stringChan + i + " ";
    } else {
      stringLe = stringLe + i + " ";
    }
  }
  document.getElementById(
    "goiy1"
  ).innerHTML = `<p class="bg-dark text-light"> ${stringChan}</p> <p class="bg-light text-dark"> ${stringLe}</p>`;
}

// Bài tập 2 BT Gợi ý
function hienThiKetQua() {
  var soChiaHetCho_3 = " ";
  var count = 0;
  for (var i = 0; i <= 1000; i += 3) {
    if (i % 3 == 0) {
      soChiaHetCho_3 = soChiaHetCho_3 + i + " ";
      count++;
    }
  }
  document.getElementById(
    "goiy2"
  ).innerText = `Số chia hết cho 3 nhỏ hơn 1000: ${count}`;
}
